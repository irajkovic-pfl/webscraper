from bs4 import BeautifulSoup
import requests
import pandas as pd

URL = "https://quotes.toscrape.com/"


def clean_string(string):
	chars_to_replace = {'“': '', '”': ''}
	for key, value in chars_to_replace.items():
		string = string.replace(key, value)
	return string


# Connect and download website
response = requests.get(URL)
soup = BeautifulSoup(response.text, "html.parser")

# Get sections from soup
quotes_section = soup.select(selector=".quote .text")
authors_section = soup.select(selector=".quote .author")
tags_section = soup.select(selector=".quote .tags")

# Parse information
collections = []
tag_links = []
for i in range(0, len(quotes_section)):
	tag_names = []
	for tag in tags_section[i].select(".tag"):
		tag_name = tag.getText()
		tag_names.append(tag.getText())
		# New tag_link dictionary
		new_tag = {
			'tag_name': tag_name,
			'tag_url': URL + tag["href"]
		}
		tag_links.append(new_tag)

	# New collection dictionary
	new_col = {
		'quote': clean_string(quotes_section[i].getText()),
		'author': authors_section[i].getText(),
		'tags': tag_names
	}
	collections.append(new_col)

# Crete DataFrames
data1 = pd.DataFrame(collections)
data2 = pd.DataFrame(tag_links)
data2.drop_duplicates()

# Save to CSV
data1.to_csv("quotes_collection.csv", index=False)
data2.to_csv("tag_links.csv", index=False)
